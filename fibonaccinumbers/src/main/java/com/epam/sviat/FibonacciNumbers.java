package com.epam.sviat;

import java.util.*;

public class FibonacciNumbers {

  public static void main(String[] arg) {
    Scanner input = new Scanner(System.in);
    int start;
    int end;
    int size;
    System.out.println("Enter the start of interval");
    start = input.nextInt();
    System.out.println("Enter the end of interval");
    end = input.nextInt();
    oddEven(start, end);
    System.out.println("Enter the size of set Fibonacci");
    size = input.nextInt();
    if (size < 2) {
      System.out.println("nono");
    } else if (end % 2 == 1) {
      fibonacciNumbers(end, end - 1, size);
    } else {
      fibonacciNumbers(end - 1, end, size);
    }
  }

  private static void oddEven(int start, int end) {
    int sumOdd = 0;
    int sumEven = 0;
    System.out.println("odd numbers - ");
    for (int i = start; i <= end; i++) {
      if (i % 2 == 1) {
        sumOdd += i;
        System.out.print(i + "  ");
      }
    }
    System.out.println();
    System.out.println("even numbers - ");
    for (int i = end; i >= start; i--) {
      if (i % 2 == 0) {
        sumEven += i;
        System.out.print(i + "  ");
      }
    }
    System.out.println("\nthe sum of odd numbers = " + sumOdd);
    System.out.println("the sum of odd numbers = " + sumEven);
  }

  private static void fibonacciNumbers(int f1, int f2, int size) {
    int[] finArray = new int[size];
    float countOdd = 1;
    finArray[0] = f1;
    finArray[1] = f2;
    for (int i = 2; i < size; i++) {
      finArray[i] = finArray[i - 1] + finArray[i - 2];
      if (finArray[i] % 2 == 1) {
        countOdd++;
      }
    }
    for (int i : finArray) {
      System.out.print(i + " ");
    }
    System.out.println("\nOdd numbers = " + (countOdd * 100) / size + "%");
    System.out.println("Even numbers = " + (100.0f - ((countOdd * 100) / size)) + "%");
  }
}
